module.exports = function(query, options, cb) {
	function get_connection() {
		require("./connect_mongo").getConnection(function(col) {
			deleteManyCollection(col);
		});
	}


	function deleteManyCollection(col) {
		var update_options = {
			j: options.j ? options.j : false
		};

		var w = options.w ? options.w : false;
		if(w) {
			update_options["w"] = w;
		}
		var wtimeout = options.wtimeout ? options.wtimeout : false;
		if(wtimeout) {
			update_options["wtimeout"] = wtimeout;
		}

		col.deleteMany(query, update_options, function(err, doc) {
			if(err) {
				throw err;
				cb(err);
			} else {
				cb(false, doc);
			}
		});
	};

	get_connection();
};