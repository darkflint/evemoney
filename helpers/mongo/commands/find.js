module.exports = function(query, fields, options, cb) {
	function get_connection() {
		require("./connect_mongo").getConnection(function(col) {
			findCollection(col);
		});
	}

	function findCollection(col) {
		var sort = {};
		if(options.sort) {
			for(var key in options.sort) {
				switch(options.sort[key]) {
					case "desc": options.sort[key] = -1;
						break;
					case "asc":
					default: options.sort[key] = 1;
				}
			}
			sort = options.sort;
		}
		var limit = options.limit ? options.limit : 0;
		var skip = options.skip ? options.skip : 0;

		col.find(query).sort(sort).limit(limit).skip(skip).project(fields).toArray(function(err, docs) {
			if(err) {
				throw err;
				cb(err);
			} else {
				if(docs.length > 0) {
					cb(false, docs);
				} else {
					cb(false, false);
				}
			}
		});
	};

	get_connection();
};