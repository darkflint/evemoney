module.exports = function(query, update, options, cb) {
	function get_connection() {
		require("./connect_mongo").getConnection(function(col) {
			findOneAndUpdateCollection(col);
		});
	}


	function findOneAndUpdateCollection(col) {
		var sort = {};
		if(options.sort) {
			for(var key in options.sort) {
				switch(options.sort[key]) {
					case "desc": options.sort[key] = -1;
						break;
					case "asc":
					default: options.sort[key] = 1;
				}
			}
			sort = options.sort;
		}

		var update_options = {
			projection: options.projection ? options.projection : {},
			sort: sort,
			upsert: options.upsert ? options.upsert : false,
			returnOriginal: options.returnOriginal ? options.returnOriginal : false
		};

		var maxTimeMS = options.maxTimeMS ? options.maxTimeMS : false;
		if(maxTimeMS) {
			update_options["maxTimeMS"] = maxTimeMS;
		}

		col.findOneAndUpdate(query, update, update_options, function(err, doc) {
			if(err) {
				throw err;
				cb(err);
			} else {
				cb(false, doc);
			}
		});
	};

	get_connection();
};