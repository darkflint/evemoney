/**
 * Count collection
 * @param query
 * @param options
 * @param cb
 */
module.exports = function(query, options, cb) {
	function get_connection() {
		require("./connect_mongo").getConnection(function(col) {
			countCollection(col);
		});
	}

	function countCollection(col) {
		var limit = options.limit ? options.limit : 0;
		var skip = options.skip ? options.skip : 0;

		col.count(query, {limit: limit, skip: skip}, function(err, counts) {
			if(err) {
				throw err;
				cb(err);
			} else {
				cb(false, counts);
			}
		});
	};

	get_connection();
};