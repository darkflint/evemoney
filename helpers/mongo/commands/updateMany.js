module.exports = function(query, update, options, cb) {
	function get_connection() {
		require("./connect_mongo").getConnection(function(col) {
			updateManyCollection(col);
		});
	}


	function updateManyCollection(col) {
		var update_options = {
			upsert: options.upsert ? options.upsert : false,
			j: options.j ? options.j : false
		};

		var w = options.w ? options.w : false;
		if(w) {
			update_options["w"] = w;
		}
		var wtimeout = options.wtimeout ? options.wtimeout : false;
		if(wtimeout) {
			update_options["wtimeout"] = wtimeout;
		}

		col.updateMany(query, update, update_options, function(err, doc) {
			if(err) {
				throw err;
				cb(err);
			} else {
				cb(false, doc);
			}
		});
	};

	get_connection();
};