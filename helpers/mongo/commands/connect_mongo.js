var db;
var col;

module.exports.startConnection = function(cb) {
	var MongoClient = require('mongodb').MongoClient;
	MongoClient.connect("mongodb://127.0.0.1:27017/eveMoney", function(err, database) {
		if(err) {
			throw err;
		}

		db = database;
		col = db.collection("eveMoney");
		cb(true);
	});
};

module.exports.getConnection = function(cb) {
	cb(col);
};